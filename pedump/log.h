#ifndef __LOG_H__
#define __LOG_H__

#define LOG_FATAL   0
#define LOG_ERR     1
#define LOG_WARN    2
#define LOG_INFO    3
#define LOG_DEBUG   4
#define LOG_DEFAULT LOG_INFO

#define INDENT "    "

void log_set_lvl(int lvl);
void log_write(int lvl, const char *fmt, ...);
void log_errcode(int lvl, int errcode, const char *usr_msg);

#endif
