#include "pehandlers.h"
#include "debugs.h"

static void guid_print(const GUID *Guid, FILE *out) {
	fprintf(out, "{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}\n",
		Guid->Data1, Guid->Data2, Guid->Data3, Guid->Data4[0], Guid->Data4[1], Guid->Data4[2],
		Guid->Data4[3], Guid->Data4[4], Guid->Data4[5], Guid->Data4[6], Guid->Data4[7]);
}

static void rsds_dump(pe_desc_t *pe_desc, LPBYTE pDebugInfo, FILE *out) {
	struct CV_INFO_PDB70 {
		DWORD      CvSignature;
		GUID       Signature;       // unique identifier 
		DWORD      Age;             // an always-incrementing value 
		BYTE       PdbFileName[1];  // zero terminated string with the name of the PDB file 
	} *pCvInfo = (struct CV_INFO_PDB70*)pDebugInfo;

	if (IsBadReadPtr(pDebugInfo, sizeof(struct CV_INFO_PDB70)))
		return;

	if (IsBadStringPtrA((CHAR*)pCvInfo->PdbFileName, UINT_MAX))
		return;

	fprintf(out, INDENT "CodeView format: RSDS \n");
	fprintf(out, INDENT "Signature: ");

	GUID dumpDirGuid = pCvInfo->Signature;
	guid_print(&dumpDirGuid, out);

	fprintf(out, INDENT "Age: %u\n", pCvInfo->Age);
	fprintf(out, INDENT "PdbFile: %s \n", pCvInfo->PdbFileName);

	if (pe_desc->dia != NULL) {
		GUID pdbGuid;
		pe_desc->dia->pGlobal->get_guid(&pdbGuid);

		BOOL guidEqual = (!memcmp(&pdbGuid, &dumpDirGuid, sizeof(GUID)));
		fprintf(out, INDENT "PDB %sMATCH\n", guidEqual ? "" : "DOESN'T ");
		if (!guidEqual)
			pe_desc->dia = NULL; // TODO: destruction needed
	}
}

static void debug_dir_dump(pe_desc_t *pe_desc, PIMAGE_DEBUG_DIRECTORY pDebugDir, FILE *out) {
	/*if (pDebugDir->Type >= IMAGE_DEBUG_TYPE_CLSID)
	return;
	*/

	PCHAR SzDebugFormats[] = {
		"UNKNOWN/BORLAND", "COFF", "CODEVIEW", "FPO", "MISC", "EXCEPTION", "FIXUP",
		"OMAP_TO_SRC", "OMAP_FROM_SRC" };
	PSTR szDebugFormat = (pDebugDir->Type <= IMAGE_DEBUG_TYPE_OMAP_FROM_SRC)
		? SzDebugFormats[pDebugDir->Type] : "???";

	fprintf(out, "%-15s %08X %08X %08X %08X %08X %u.%02u\n",
		szDebugFormat, pDebugDir->SizeOfData, pDebugDir->AddressOfRawData,
		pDebugDir->PointerToRawData, pDebugDir->Characteristics,
		pDebugDir->TimeDateStamp, pDebugDir->MajorVersion,
		pDebugDir->MinorVersion);

	if (pDebugDir->Type == IMAGE_DEBUG_TYPE_CODEVIEW) {
		LPBYTE pDebugInfo = (LPBYTE)((LPBYTE)pe_desc->pImageBase + pDebugDir->AddressOfRawData);
		DWORD debugInfoSize = pDebugDir->SizeOfData;

		DWORD* smth = (DWORD*)pDebugInfo;

		DWORD CvSignature = *(DWORD*)pDebugInfo;

		const DWORD cv_signature_rsds = 'SDSR';
		const DWORD cv_signature_nb10 = '01BN';

		if (CvSignature == cv_signature_nb10) {
			log_write(LOG_WARN, "PDB 2.0 is not supported yet");
		}
		else if (CvSignature == cv_signature_rsds) {
			// RSDS -> PDB 7.00 
			rsds_dump(pe_desc, pDebugInfo, out);
		}
		else {
			log_write(LOG_WARN, "Unexpected debug info type: %04d", CvSignature);
		}
	}
}

void debug_dump(pe_desc_t *pe_desc, FILE *out) {
	fprintf(out, "Debug directory:\n");

	DWORD debugRVA = data_dir_RVA(pe_desc, IMAGE_DIRECTORY_ENTRY_DEBUG),
		debugSize = data_dir_size(pe_desc, IMAGE_DIRECTORY_ENTRY_DEBUG);
	if (!debugRVA || !debugSize)
		return;

	PIMAGE_DEBUG_DIRECTORY pDebugDir = (PIMAGE_DEBUG_DIRECTORY)((PCHAR)pe_desc->pImageBase + debugRVA);

	int numEntries = debugSize / sizeof(IMAGE_DEBUG_DIRECTORY);

	fprintf(out, "# debug sections %u\n", numEntries);

	for (int i = 0; i < numEntries; i++)
		debug_dir_dump(pe_desc, &pDebugDir[i], out);
}