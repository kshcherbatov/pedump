#pragma once

#include <windows.h>
#include <stdio.h>
#include <dia2.h>
#include <atlbase.h>
#include <Shlwapi.h>
#include "log.h"

typedef struct {
	LPSTR fileName;
	CComPtr<IDiaDataSource> pSource;
	CComPtr<IDiaSession>    pSession;
	CComPtr<IDiaSymbol>     pGlobal;
	CComPtr<IDiaEnumTables> pTables;
} dia_desc_t;

typedef struct {
	LPCSTR fileName;
	HANDLE hFile;
	HANDLE hFileMapping;
	PVOID  pImageBase;
	BOOL   is64bit;
	union {
		struct {
			PIMAGE_NT_HEADERS32 NTHeader32;
			PIMAGE_NT_HEADERS64 NTHeader64;
		};
		void * NTHeader[2];
	};

	dia_desc_t *dia;
} pe_desc_t;

#define pNTHeader(pe_desc, TYPE) ((TYPE)pe_desc->NTHeader[pe_desc->is64bit])

static inline DWORD data_dir_RVA(const pe_desc_t *pe_desc, int dir_idx) {
	if( pe_desc->is64bit )
		return pe_desc->NTHeader64->OptionalHeader.DataDirectory[dir_idx].VirtualAddress;
	else return pe_desc->NTHeader32->OptionalHeader.DataDirectory[dir_idx].VirtualAddress;
}

static inline DWORD data_dir_size(const pe_desc_t *pe_desc, int dir_idx) {
	if (pe_desc->is64bit)
		return pe_desc->NTHeader64->OptionalHeader.DataDirectory[dir_idx].Size;
	else return pe_desc->NTHeader32->OptionalHeader.DataDirectory[dir_idx].Size;
}

void dia_init_or_die(LPCSTR fileFullName, dia_desc_t *dia);
void pedump(LPCSTR fileName, dia_desc_t *dia, FILE *out);