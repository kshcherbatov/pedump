#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>

#include "log.h"

static int log_level = LOG_DEFAULT;

static const char *prefix[] = {
	"\x1b[31m[FATAL]\x1b[0m ",
	"\x1b[31m[ERROR]\x1b[0m ",
	"\x1b[33m[WARN]\x1b[0m  ",
	"\x1b[32m[INFO]\x1b[0m  ",
	"\x1b[35m[DEBUG]\x1b[0m "
};

#define MSG_SZ 256

void log_set_lvl(int lvl) {
	log_level = lvl;
}

void log_write(int lvl, const char *fmt, ...) {
	if(fmt == NULL || lvl > log_level)
		return;

	char msg_buf[MSG_SZ];
	size_t msg_len = sprintf(msg_buf, "%s", prefix[lvl]);

	va_list va;
	va_start(va, fmt);
	msg_len += vsnprintf(msg_buf + msg_len, MSG_SZ - msg_len, fmt, va);
	va_end(va);

	fprintf(stderr, "%.*s\n", (int)msg_len, msg_buf);

	if (lvl == LOG_FATAL)
		exit(EXIT_FAILURE);
}

void log_errcode(int lvl, int errcode, const char *usr_msg) {
	if (usr_msg == NULL || lvl > log_level)
		return;

	char msg_buf[MSG_SZ];
	size_t msg_len = sprintf(msg_buf, "%s%s: ", prefix[lvl], usr_msg);

	msg_len += FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		errcode,
		MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT),
		(LPSTR)(msg_buf+msg_len),
		(int)(MSG_SZ - msg_len),
		NULL
	);
	fprintf(stderr, "%.*s\n", (int)msg_len, msg_buf);

	if (lvl == LOG_FATAL)
		exit(EXIT_FAILURE);
}