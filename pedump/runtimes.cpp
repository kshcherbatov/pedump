#include "pehandlers.h"
#include "unwind.h"
#include "runtimes.h"
#include <assert.h>

static void func_sym_dump(const pe_desc_t *pe_desc, DWORD RVA, FILE *out) {
	assert(pe_desc->dia);

	CComPtr<IDiaSymbol> symbol;
	if (pe_desc->dia->pSession->findSymbolByRVA(RVA, SymTagFunction, &symbol) != S_OK) {
		log_write(LOG_ERR, "findSymbolByRVA");
		return;
	}
	if (!symbol) {
		log_write(LOG_ERR, "symbol not found");
		return;
	}
	BSTR name;
	if (symbol->get_name(&name) != S_OK) {
		log_write(LOG_ERR, "get_name");
		return;
	}

	fprintf(out, "%S", name);
}

static void unwind_info_dump(const pe_desc_t *pe_desc, PRUNTIME_FUNCTION function, FILE *out) {
	PUNWIND_INFO pUnwindInfo = (PUNWIND_INFO)((PCHAR)pe_desc->pImageBase + function->UnwindData);
	if (pUnwindInfo->Version != 1) {
		log_write(LOG_DEBUG, "    *** unknown version %u\n", pUnwindInfo->Version);
		return;
	}

	static const char * const reg_names[16] =
	{ "rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi",
	"r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15" };

	fprintf(out, "\nFunction %08x-%08x", function->BeginAddress, function->EndAddress);
	if (pe_desc->dia) {
		fprintf(out, " (SYMBOL: ");
		func_sym_dump(pe_desc, function->BeginAddress, out);
		fprintf(out, ")");
	}

	if (function->UnwindData & 1) {
		PRUNTIME_FUNCTION next = (PRUNTIME_FUNCTION)((PCHAR)pe_desc->pImageBase + (function->UnwindData & ~1));
		fprintf(out, "  -> function %08x-%08x\n", next->BeginAddress, next->EndAddress);
		return;
	}

	fprintf(out, "    flags %x", pUnwindInfo->Flags);
	if (pUnwindInfo->Flags & UNW_FLAG_EHANDLER) fprintf(out, " EHANDLER");
	if (pUnwindInfo->Flags  & UNW_FLAG_UHANDLER) fprintf(out, " UHANDLER");
	if (pUnwindInfo->Flags & UNW_FLAG_CHAININFO) fprintf(out, " CHAININFO");
	fprintf(out, "\n    prolog 0x%x bytes\n", pUnwindInfo->SizeOfProlog);
	if (pUnwindInfo->FrameRegister)
		fprintf(out, "    frame register %s offset 0x%x(%%rsp)\n", reg_names[pUnwindInfo->FrameRegister], pUnwindInfo->FrameOffset * 16);

	unsigned count = 0;
	for (int i = 0; i < pUnwindInfo->CountOfCodes; i++) {
		fprintf(out, "      0x%02x: ", pUnwindInfo->UnwindCode[i].CodeOffset);
		switch (pUnwindInfo->UnwindCode[i].UnwindOp)
		{
		case UWOP_PUSH_NONVOL:
			fprintf(out, "push %%%s\n", reg_names[pUnwindInfo->UnwindCode[i].OpInfo]);
			break;
		case UWOP_ALLOC_LARGE:
			if (pUnwindInfo->UnwindCode[i].OpInfo) {
				count = *(DWORD *)&pUnwindInfo->UnwindCode[i + 1];
				i += 2;
			}
			else {
				count = *(USHORT *)&pUnwindInfo->UnwindCode[i + 1] * 8;
				i++;
			}
			fprintf(out, "sub $0x%x,%%rsp\n", count);
			break;
		case UWOP_ALLOC_SMALL:
			count = (pUnwindInfo->UnwindCode[i].OpInfo + 1) * 8;
			fprintf(out, "sub $0x%x,%%rsp\n", count);
			break;
		case UWOP_SET_FPREG:
			fprintf(out, "lea 0x%x(%%rsp),%s\n",
				pUnwindInfo->FrameOffset * 16, reg_names[pUnwindInfo->FrameRegister]);
			break;
		case UWOP_SAVE_NONVOL:
			count = *(USHORT *)&pUnwindInfo->UnwindCode[i + 1] * 8;
			fprintf(out, "mov %%%s,0x%x(%%rsp)\n", reg_names[pUnwindInfo->UnwindCode[i].OpInfo], count);
			i++;
			break;
		case UWOP_SAVE_NONVOL_FAR:
			count = *(DWORD *)&pUnwindInfo->UnwindCode[i + 1];
			fprintf(out, "mov %%%s,0x%x(%%rsp)\n", reg_names[pUnwindInfo->UnwindCode[i].OpInfo], count);
			i += 2;
			break;
		case UWOP_SAVE_XMM128:
			count = *(USHORT *)&pUnwindInfo->UnwindCode[i + 1] * 16;
			fprintf(out, "movaps %%xmm%u,0x%x(%%rsp)\n", pUnwindInfo->UnwindCode[i].OpInfo, count);
			i++;
			break;
		case UWOP_SAVE_XMM128_FAR:
			count = *(DWORD *)&pUnwindInfo->UnwindCode[i + 1];
			fprintf(out, "movaps %%xmm%u,0x%x(%%rsp)\n", pUnwindInfo->UnwindCode[i].OpInfo, count);
			i += 2;
			break;
		case UWOP_PUSH_MACHFRAME:
			fprintf(out, "PUSH_MACHFRAME %u\n", pUnwindInfo->UnwindCode[i].OpInfo);
			break;
		default:
			fprintf(out, "*** unknown code %u\n", pUnwindInfo->UnwindCode[i].UnwindOp);
			break;
		}
	}

	union handler_data_t {
		RUNTIME_FUNCTION chain;
		DWORD handler;
	}  *handler_data = (union handler_data_t *)&pUnwindInfo->UnwindCode[(pUnwindInfo->CountOfCodes + 1) & ~1];

	if (pUnwindInfo->Flags & UNW_FLAG_CHAININFO) {
		fprintf(out, "    -> function %08x-%08x\n", handler_data->chain.BeginAddress, handler_data->chain.EndAddress);
		return;
	}
	if (pUnwindInfo->Flags & (UNW_FLAG_EHANDLER | UNW_FLAG_UHANDLER)) {
		DWORD handlerDataRVA = function->UnwindData + DWORD((char *)(&handler_data->handler + 1) - (char *)pUnwindInfo);
		fprintf(out, "    handler %08x data at %08x\n", handler_data->handler, handlerDataRVA);
	}
}

void runtime_functions_dump(pe_desc_t *pe_desc, FILE *out)
{
	fprintf(out, "Runtimes directory:\n");


	DWORD exceptRVA = data_dir_RVA(pe_desc, IMAGE_DIRECTORY_ENTRY_EXCEPTION),
		cEntries = data_dir_size(pe_desc, IMAGE_DIRECTORY_ENTRY_EXCEPTION) / sizeof(IMAGE_RUNTIME_FUNCTION_ENTRY);
	if (!exceptRVA || !cEntries)
		return;

	fprintf(out, "cEntries = %u\n", cEntries);
	PIMAGE_RUNTIME_FUNCTION_ENTRY pRTFn = (PIMAGE_RUNTIME_FUNCTION_ENTRY)((PCHAR)pe_desc->pImageBase + exceptRVA);
	for (unsigned i = 0; i < cEntries; i++)
		unwind_info_dump(pe_desc, &pRTFn[i], out);
}