#pragma once

template<class THUNK_T>
void _imports_dump(pe_desc_t *pe_desc, FILE *out) {
	fprintf(out, "Import table:\n"); 

	DWORD importRVA = data_dir_RVA(pe_desc, IMAGE_DIRECTORY_ENTRY_IMPORT);
	PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)((PCHAR)pe_desc->pImageBase + importRVA);

	while (pImportDescriptor->Name != 0 || pImportDescriptor->TimeDateStamp != 0) {
		fprintf(out, INDENT "DLL name: %s\n", (char*)pe_desc->pImageBase + (DWORD)pImportDescriptor->Name);
		fprintf(out, INDENT " ordinal | ordname\n");

		THUNK_T thunk = (THUNK_T)((char*)pe_desc->pImageBase + pImportDescriptor->Characteristics);
		THUNK_T thunkIAT = (THUNK_T)((char*)pe_desc->pImageBase + pImportDescriptor->FirstThunk);

		if (thunk == 0)
			thunk = thunkIAT;

		while (thunk && thunk->u1.AddressOfData != 0) {
			if (thunk->u1.AddressOfData == 0)
				break;

			if (thunk->u1.Ordinal & IMAGE_ORDINAL_FLAG) {
				fprintf(out, INDENT "%8llu\n", (ULONGLONG)IMAGE_ORDINAL(thunk->u1.Ordinal));
			}
			else {
				PIMAGE_IMPORT_BY_NAME pOrdinalName = (PIMAGE_IMPORT_BY_NAME)((PCHAR)pe_desc->pImageBase +
					(DWORD)thunk->u1.AddressOfData);
				fprintf(out, INDENT "%8u | %s\n", pOrdinalName->Hint, pOrdinalName->Name);
			}

			thunk++;
		}
		fprintf(out, "\n");
		pImportDescriptor++;
	}
}

int header_dump(pe_desc_t *pe_desc, FILE *out);

#define imports_dump(pe_desc, out)   _imports_dump<PIMAGE_THUNK_DATA64>(pe_desc, out)
#define imports86_dump(pe_desc, out) _imports_dump<PIMAGE_THUNK_DATA32>(pe_desc, out)

void exports_dump(pe_desc_t *pe_desc, FILE *out);