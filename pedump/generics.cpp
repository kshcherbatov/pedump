#include "pehandlers.h"
#include "generics.h"

int header_dump(pe_desc_t *pe_desc, FILE *out) {
	fprintf(out, "PE Header:\n");

	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)pe_desc->pImageBase;
	fprintf(out, "DOS HEADER = %c%c 0x%x\n",
		pDosHeader->e_magic & 0xFF,
		(pDosHeader->e_magic >> 8) & 0xFF,
		pDosHeader->e_lfanew
		);
	if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE) {
		fprintf(out, "It is not PE!\n");
		return NULL;
	}

	PIMAGE_NT_HEADERS64 pPeHeader = (PIMAGE_NT_HEADERS64)(((unsigned char*)pe_desc->pImageBase + pDosHeader->e_lfanew));
	fprintf(out, "PE HEADER = #%c%c%x%x# 0x%x\n",
		(pPeHeader->Signature) & 0xFF,
		(pPeHeader->Signature >> 8) & 0xFF,
		(pPeHeader->Signature >> 16) & 0xFF,
		(pPeHeader->Signature >> 24) & 0xFF,
		pPeHeader->FileHeader.Machine);
	
	BOOL op_header_has_valid_size = FALSE;

	if (pPeHeader->FileHeader.Machine == IMAGE_FILE_MACHINE_I386) {
		op_header_has_valid_size = (pPeHeader->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER32));
		pe_desc->is64bit = 0;
	}
	else if (pPeHeader->FileHeader.Machine == IMAGE_FILE_MACHINE_AMD64) {
		op_header_has_valid_size = (pPeHeader->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER64));
		pe_desc->is64bit = 1;
	}
	else return -1;

	pe_desc->NTHeader[pe_desc->is64bit] = pPeHeader;

	fprintf(out, "%s\n",
		op_header_has_valid_size
		? "Optional header has a valid size"
		: "Optional header has an INVALID size"
	);
	if (!op_header_has_valid_size || pPeHeader->Signature != IMAGE_NT_SIGNATURE) {
		fprintf(out, "It is not NT-compatable file!\n");
		return -1;
	}

	return 0;
}

void exports_dump(pe_desc_t *pe_desc, FILE *out) {
	fprintf(out, "Export table:\n");

	DWORD exportRVA = data_dir_RVA(pe_desc, IMAGE_DIRECTORY_ENTRY_EXPORT),
		exportSize = data_dir_size(pe_desc, IMAGE_DIRECTORY_ENTRY_EXPORT);

	PIMAGE_EXPORT_DIRECTORY exportDir = (PIMAGE_EXPORT_DIRECTORY)((CHAR *)pe_desc->pImageBase + exportRVA);
	DWORD exportDirStart = exportRVA;
	DWORD exportDirEnd = (exportRVA + exportSize);
	if (!exportRVA || !exportSize)
		return;

	fprintf(out, INDENT "Name:            %s\n", (PSTR)((PCHAR)pe_desc->pImageBase + exportDir->Name));
	fprintf(out, INDENT "Version:         %u.%02u\n", exportDir->MajorVersion, exportDir->MinorVersion);
	fprintf(out, INDENT "Ordinal base:    %08X\n", exportDir->Base);
	fprintf(out, INDENT "# of functions:  %08X\n", exportDir->NumberOfFunctions);
	fprintf(out, INDENT "# of Names:      %08X\n", exportDir->NumberOfNames);

	PDWORD functions = (PDWORD)((PCHAR)pe_desc->pImageBase + exportDir->AddressOfFunctions);
	PWORD ordinals = (PWORD)((PCHAR)pe_desc->pImageBase + exportDir->AddressOfNameOrdinals);
	PDWORD namePtr = (PDWORD)((PCHAR)pe_desc->pImageBase + exportDir->AddressOfNames);

	fprintf(out, INDENT "Func     | Ordn | Name\n");

	for (DWORD i = 0; i < exportDir->NumberOfFunctions; i++) {
		DWORD entryPointRVA = functions[i];
		if (entryPointRVA == 0)
			continue;

		fprintf(out, INDENT "%08X | %4u |", entryPointRVA, i + exportDir->Base);

		// See if this function has an associated name exported for it.
		for (DWORD j = 0; j < exportDir->NumberOfNames; j++)
			if (ordinals[j] == i) {
				PCSTR name = (PCHAR)pe_desc->pImageBase + namePtr[j];
				fprintf(out, "  %s", name);
			}

		if (exportDirStart <= entryPointRVA && entryPointRVA <= exportDirEnd)
			fprintf(out, " (forwarder -> %s)", (PCHAR)pe_desc->pImageBase + entryPointRVA);

		fprintf(out, "\n");
	}
}