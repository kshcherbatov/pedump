#define _CRT_SECURE_NO_WARNINGS

#include "pehandlers.h"
#include "generics.h"
#include "sections.h"
#include "debugs.h"

void runtime_functions_dump(pe_desc_t *pe_desc, FILE *out);

void dia_init_or_die(LPCSTR fileFullName, dia_desc_t *dia) {
	dia->fileName = StrDupA(fileFullName);
	PathStripPathA(dia->fileName);

	if (FAILED(CoInitialize(NULL))) {
		log_write(LOG_FATAL, "CoInitialize");
		return;
	}

	HRESULT hr = CoCreateInstance(CLSID_DiaSource,
		NULL,
		CLSCTX_INPROC_SERVER,
		__uuidof(IDiaDataSource),
		(void **)&dia->pSource);
	if (FAILED(hr))
		log_write(LOG_FATAL, "Could not CoCreate CLSID_DiaSource. Register msdia80.dll");

	wchar_t wszFilename[MAX_PATH];
	mbstowcs(wszFilename, fileFullName, sizeof(wszFilename) / sizeof(wszFilename[0]));

	if (FAILED(dia->pSource->loadDataFromPdb(wszFilename)))
		log_write(LOG_FATAL, "loadDataFromPdb");

	if (FAILED(dia->pSource->openSession(&dia->pSession)))
		log_write(LOG_FATAL, "openSession");

	if (FAILED(dia->pSession->get_globalScope(&dia->pGlobal)))
		log_write(LOG_FATAL, "get_globalScope");

	if (FAILED(dia->pSession->getEnumTables(&dia->pTables)))
		log_write(LOG_FATAL, "getEnumTables");
}

static void separator(FILE *out) {
	fprintf(out, "\n\n-------------------------------------------------------------------\n");
}

void pedump(LPCSTR fileName, dia_desc_t *dia, FILE *out) {
	pe_desc_t pe_desc;

	pe_desc.fileName = fileName;
	pe_desc.dia = dia;

	fprintf(out, "Processing File %s\n\n", fileName);

	pe_desc.hFile = CreateFileA(fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (pe_desc.hFile == INVALID_HANDLE_VALUE)
		log_errcode(LOG_FATAL, GetLastError(), "CreateFile");

	pe_desc.hFileMapping = CreateFileMapping(pe_desc.hFile, NULL, PAGE_READONLY | SEC_IMAGE, 0, 0, NULL);
	if (pe_desc.hFileMapping == NULL) {
		log_errcode(LOG_ERR, GetLastError(), "CreateFileMapping");
		goto _exit;
	}

	pe_desc.pImageBase = MapViewOfFile(pe_desc.hFileMapping, FILE_MAP_READ, 0, 0, 0);
	if (pe_desc.pImageBase == NULL) {
		log_errcode(LOG_ERR, GetLastError(), "MapViewOfFile");
		goto _exit;
	}

	if (header_dump(&pe_desc, out))
		goto _exit;

	typedef void(__cdecl *f_debug)(pe_desc_t*, FILE*);
	f_debug todo_list[] = { sections_dump, debug_dump, exports_dump };

	for (int i = 0; i < sizeof(todo_list) / sizeof(f_debug); i++) {
		separator(out);
		todo_list[i](&pe_desc, out);
	}

	separator(out);
	if (pe_desc.is64bit) {
		imports_dump(&pe_desc, out);
		separator(out);
		runtime_functions_dump(&pe_desc, out);
	}
	else {
		imports86_dump(&pe_desc, out);
	}

_exit:
	if (pe_desc.pImageBase)
		UnmapViewOfFile(pe_desc.pImageBase);
	if (pe_desc.hFileMapping)
		CloseHandle(pe_desc.hFileMapping);
	if (pe_desc.hFile != INVALID_HANDLE_VALUE)
		CloseHandle(pe_desc.hFile);
}