#include "pehandlers.h"
#include "sections.h"

typedef struct {
	DWORD val;
	const char *name;
} char_desc_t;

static const char_desc_t sec_flag_names[] = {
	{ IMAGE_SCN_TYPE_NO_PAD, "NO_PAD" },
	{ IMAGE_SCN_CNT_CODE, "CODE" },
	{ IMAGE_SCN_CNT_INITIALIZED_DATA, "INITIALIZED_DATA" },
	{ IMAGE_SCN_CNT_UNINITIALIZED_DATA, "UNINITIALIZED_DATA" },
	{ IMAGE_SCN_LNK_OTHER, "OTHER" },
	{ IMAGE_SCN_LNK_INFO, "INFO" },
	{ IMAGE_SCN_LNK_REMOVE, "REMOVE" },
	{ IMAGE_SCN_LNK_COMDAT, "COMDAT" },
	{ IMAGE_SCN_MEM_FARDATA, "FARDATA" },
	{ IMAGE_SCN_MEM_PURGEABLE, "PURGEABLE" },
	{ IMAGE_SCN_MEM_LOCKED, "LOCKED" },
	{ IMAGE_SCN_MEM_PRELOAD, "PRELOAD" },
	{ IMAGE_SCN_LNK_NRELOC_OVFL, "NRELOC_OVFL" },
	{ IMAGE_SCN_MEM_DISCARDABLE, "DISCARDABLE" },
	{ IMAGE_SCN_MEM_NOT_CACHED, "NOT_CACHED" },
	{ IMAGE_SCN_MEM_NOT_PAGED, "NOT_PAGED" },
	{ IMAGE_SCN_MEM_SHARED, "SHARED" },
	{ IMAGE_SCN_MEM_EXECUTE, "EXECUTE" },
	{ IMAGE_SCN_MEM_READ, "READ" },
	{ IMAGE_SCN_MEM_WRITE, "WRITE" }
};

static void section_dump(PIMAGE_SECTION_HEADER hdr, FILE *out) {
	fprintf(out, "SECTION %8.*s\n", IMAGE_SIZEOF_SHORT_NAME, (char *)hdr->Name);
	fprintf(out, "RVA = 0x%08x, size of raw data = 0x%08x\n", hdr->VirtualAddress, hdr->SizeOfRawData);

	fprintf(out, "Section characteristics is following:\n");

	for (int i = 0; i < sizeof(sec_flag_names) / sizeof(char_desc_t); i++)
		if (hdr->Characteristics & sec_flag_names[i].val)
			fprintf(out, INDENT "%s", sec_flag_names[i].name);
	fprintf(out, "\n\n");
}

void sections_dump(pe_desc_t *pe_desc, FILE *out) {
	fprintf(out, "PE Sections:\n");

	PIMAGE_SECTION_HEADER pSectionHeader = IMAGE_FIRST_SECTION(pNTHeader(pe_desc, PIMAGE_NT_HEADERS));
	for (int i = 0; i < pNTHeader(pe_desc, PIMAGE_NT_HEADERS)->FileHeader.NumberOfSections; i++)
		section_dump(pSectionHeader++, out);
}