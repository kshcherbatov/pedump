#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <stdio.h>

#include "getopt.h"
#include "log.h"
#include "pehandlers.h"

static void print_help_and_die(const char * const program_name, FILE * const stream, const int exit_code) {
	fprintf(stream,
		"Usage: %s [options]\n"
		"   -h   --help                       Display this usage information\n"
		"   -d   --log-level     lvl          Specify log level (number)\n"
		"   -f   --file          filename     Specify input object file\n"
		"   -o   --output        filename     Set output file; default is STDOUT\n"
		"   -p   --pdb           filename     Specify PDB file\n",
		program_name
	);
	exit(exit_code);
}

static void parse_cmd_options_or_die(int argc, char *argv[], LPCSTR *filename, FILE **out, LPCSTR *pdb) {
	const char * const short_options = "hd:f:p:o:";
	const struct option long_options[] = {
		{ "help",    0, NULL, 'h' },
		{ "output",  1, NULL, 'o' },
		{ "file",    1, NULL, 'f' },
		{ "pdb",     1, NULL, 'p' },
		{ "log-lvl", 1, NULL, 'd' },
		{ NULL,      0, NULL,  0 }
	};

	*out = stdout;
	*pdb = NULL;

	int ch = 0;
	while ((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1) {
		switch (ch) {
		case 'h':
			print_help_and_die(argv[0], stdout, EXIT_SUCCESS);
		case 'd':
			log_set_lvl(atoi(optarg));
			break;
		case 'o':
			*out = fopen(optarg, "w");
			if (*out == NULL)
				log_write(LOG_FATAL, "open file %s failed\n", optarg);
			break;
		case 'p':
			*pdb = optarg;
			break;
		case 'f':
			*filename = optarg;
			break;
		default:
			print_help_and_die(argv[0], stderr, EXIT_FAILURE);
		}
	}
}

__declspec(noinline) int TestExceptionHandler(void) {
	printf("Exception");
	return 0;
}

void seh_test(void) {
	char temp[512];
	__try {
		strcpy(temp, temp);
	}
	__except (TestExceptionHandler()) {}
}

int main(int argc, char *argv[]) {
	FILE *out;
	LPCSTR fileName, pdbName;
	fileName = "C:\\Windows\\SysWOW64\\Kernel32.dll";
	parse_cmd_options_or_die(argc, argv, &fileName, &out, &pdbName);
	if (fileName == NULL)
		log_write(LOG_FATAL, "No input file specified");
	seh_test();

	if (pdbName != NULL) {
		dia_desc_t dia;
		dia_init_or_die(pdbName, &dia);
		pedump(fileName, &dia, out);
	}
	else {
		pedump(fileName, NULL, out);
	}

	fclose(out);
	return 0;
}